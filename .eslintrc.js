module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true,
    },
    extends: 'airbnb-base',
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    rules: {
        semi: 'warn',
        'import/extensions': 0,
        'import/no-resolved': 0,
        'no-await-in-loop': 0,
        indent: 'off',
    },
    ignorePatterns: ['playwright-report/'],
    overrides: [
        {
            files: ['*.spec.js'],
            env: {
                node: true,
            },
            rules: {
                'no-unused-expressions': [0],
            },
        },
    ],
};
