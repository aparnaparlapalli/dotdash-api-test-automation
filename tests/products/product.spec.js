const { expect } = require('@playwright/test');
const { test } = require('../../src/fixtures/baseApi');
const getSchema = require('../../src/util/schema/get-all-products-schema');
const getQueryParamSchema = require('../../src/util/schema/get-produts-by-query-params');

const categoriesData = require('../data/product-category-data');

const { categories } = categoriesData;
const id = 2;

test.describe('GET all products /products', () => {
    test('should get all products', async ({ productService, validateSchema }) => {
        await test.step('get products', async () => {
            const response = await productService.getAllProducts();
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
            expect(respBody.products.length).toBeGreaterThanOrEqual(1);

            // validate the entire schema
            const schemaIsValid = validateSchema(getSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);
        });
    });
});

test.describe('GET all products by categories products/category/categoryName', () => {
    // Parameterized categories test case
    categories.forEach((category) => {
        test(`should get all products by category products/category/"${category}"`, async ({ productService, validateSchema }) => {
            await test.step(`get by category products/category/"${category}"`, async () => {
                const response = await productService.getProductByCategory(category);
                const respBody = await response.json();
                const expectedStatus = 200;
                expect(response.status()).toBe(expectedStatus);
                expect(respBody.products.length).toBeGreaterThanOrEqual(1);

                // validate the entire schema
                const schemaIsValid = validateSchema(getSchema.schema, respBody);
                expect(schemaIsValid).toBe(true);

                // validate category in response.
                const categoryCheck = respBody.products.every((item) => item.category === category);
                expect(categoryCheck).toBeTruthy();
            });
        });
    });
});

test.describe('GET product categories products/categories', () => {
    test('should get product categories', async ({ productService }) => {
        await test.step('get product categories', async () => {
            const response = await productService.getProductCategories();
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
            expect(respBody.length).toBeGreaterThanOrEqual(1);

            // validate all categories
            expect(respBody).toEqual(categoriesData.allCategories);
        });
    });
});

test.describe('GET product by id /products/id', () => {
    test('should get product by id', async ({ productService }) => {
        await test.step('get product by id', async () => {
            const response = await productService.getProductById(id);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate the id
            expect(respBody.id).toBe(id);
        });
    });
});

// negative scenario
test.describe('GET product by id negative scenario /products/id', () => {
    test('get a product that do not exist', async ({ productService }) => {
        await test.step('get product by id', async () => {
            const productId = '9999';
            const response = await productService.getProductById(productId);
            await response.json();
            const expectedStatus = 404;
            expect(response.status()).toBe(expectedStatus);
        });
    });
});

test.describe('GET products products?limit=<limit>&skip=<skip>&select=select', () => {
    test('should get products by passing limit=10&skip=2&select=title,price', async ({ productService, validateSchema }) => {
        await test.step('get product by passing limit, select=title,price', async () => {
            const limit = 10;
            const skip = 2;
            const select = 'title,price';
            const response = await productService.getProductByQueryParam(limit, skip, select);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate the entire schema
            const schemaIsValid = validateSchema(getQueryParamSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);

            // validate object length
            expect(Object.keys(respBody.products[1])).toHaveLength(3);
            expect(respBody.limit).toBe(limit);
            expect(respBody.skip).toBe(skip);
        });
    });
});

test.describe('GET search product by search text', () => {
    test('should seacrh product', async ({ productService, validateSchema }) => {
        await test.step('get product by search text', async () => {
            const searchString = 'phone';
            const response = await productService.searchProduct(searchString);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate the entire schema
            const schemaIsValid = validateSchema(getSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);

            // validate category in response.
            // validate category in response.
            const categoryCheck = respBody.products.every(
                (item) => item.category.toLowerCase().includes(searchString),
            );
            if (categoryCheck) {
                console.log(`All products are in a "${searchString}" category.`);
            } else {
                console.log(`Not all products are in a "${searchString}" category.`);
            }
        });
    });
});
