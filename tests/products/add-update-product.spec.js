const { expect } = require('@playwright/test');
const { test } = require('../../src/fixtures/baseApi');
const addProductSchema = require('../../src/util/schema/add-product-schema');
const addProductData = require('../data/add-product.data');

let id;
test.describe('POST /products/add', () => {
    test('should add a product', async ({ productService, validateSchema }) => {
        await test.step('add product', async () => {
            const response = await productService.postProduct(addProductData.payload);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
            id = respBody.id;
            // validate the entire schema
            const schemaIsValid = validateSchema(addProductSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);
        });
        await test.step('delete a product that do not exists', async () => {
            const response = await productService.deleteProduct(id);
            await response.json();
            const expectedStatus = 404;
            expect(response.status()).toBe(expectedStatus);
        });
        await test.step('delete a product that exist', async () => {
            const response = await productService.deleteProduct(2);
            await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
        });
    });
});

test.describe('PUT /products/id', () => {
    test('should update a product', async ({ productService }) => {
        await test.step('update a product', async () => {
            const response = await productService.putProduct(1, addProductData.updatePayload);
            await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
        });
    });
});
