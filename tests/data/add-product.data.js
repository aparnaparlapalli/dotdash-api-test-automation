module.exports = {
    payload: {
        title: 'iPhone 9',
        description: 'test add product 1',
        price: 999,
        discountPercentage: 10.09,
        rating: 5.01,
        stock: 11,
        brand: 'Apple',
        category: 'smartphones',
        thumbnail: 'https://i.dummyjson.com/data/products/1/thumbnail.jpg',
        images: [
            'https://i.dummyjson.com/data/products/1/1.jpg',
        ],
    },
    updatePayload: {
        title: 'test update iPhone',
    },
};
