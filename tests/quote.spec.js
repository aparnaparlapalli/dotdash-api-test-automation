const { expect } = require('@playwright/test');
const { test } = require('../src/fixtures/baseApi');
const getSchema = require('../src/util/schema/get-all-quotes-scehma');

test.describe('GET all quotes /quotes', () => {
    test('should get all quotes', async ({ quoteService, validateSchema }) => {
        await test.step('get random quote', async () => {
            const response = await quoteService.getAllQuotes();
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
            expect(respBody.quotes.length).toBeGreaterThanOrEqual(1);

            // validate the entire schema
            const schemaIsValid = validateSchema(getSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);
        });
    });
});

test.describe('GET random quote /quotes/random', () => {
    test('should get random quote', async ({ quoteService }) => {
        await test.step('get random quote', async () => {
            const response = await quoteService.getRandomQuote();
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
            expect(respBody).toBe.Object;

            // validate object
            expect(Object.keys(respBody)).toHaveLength(3);
            expect(Object.keys(respBody)).toContain('id');
            expect(Object.keys(respBody)).toContain('quote');
            expect(Object.keys(respBody)).toContain('author');
        });
    });
});

test.describe('GET limit and skip quotes /quotes?limit=3&skip=10', () => {
    test('should limit and skip quotes', async ({ quoteService }) => {
        await test.step('get quotes', async () => {
            const limit = 2;
            const skip = 10;
            const response = await quoteService.getLimitAndSkipQuotes(limit, skip);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate object
            expect(respBody.quotes.length).toBe(limit);
            expect(respBody.limit).toBe(limit);
            expect(respBody.skip).toBe(skip);
        });
    });
});

test.describe('GET quote by id /quotes/id negative scenario', () => {
    test('get invalid quote by id', async ({ quoteService }) => {
        await test.step('get quote by id', async () => {
            const id = 'kkkkk';
            const response = await quoteService.getQuoteById(id);
            await response.json();
            const expectedStatus = 404;
            expect(response.status()).toBe(expectedStatus);
        });
    });
});
