const { expect } = require('@playwright/test');
const { test } = require('../src/fixtures/baseApi');
const getSchema = require('../src/util/schema/get-all-carts-schema');
const getByIdSchema = require('../src/util/schema/get-cart-by-id-schema');

const id = 2;

test.describe('GET all carts /carts', () => {
    test('should get all carts', async ({ cartService, validateSchema }) => {
        await test.step('get carts', async () => {
            const response = await cartService.getAllCarts();
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
            expect(respBody.carts.length).toBeGreaterThanOrEqual(1);

            // validate the entire schema
            const schemaIsValid = validateSchema(getSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);
        });
    });
});

test.describe('GET a single cart /cart/id', () => {
    test('should get a cart by id', async ({ cartService, validateSchema }) => {
        await test.step('get cart by id', async () => {
            const response = await cartService.getCartById(id);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate the entire schema
            const schemaIsValid = validateSchema(getByIdSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);

            // validate the id
            expect(respBody.id).toBe(id);
        });
    });
});

test.describe('GET carts of user /carts/user/id', () => {
    test('should get carts of given user', async ({ cartService, validateSchema }) => {
        await test.step('get cart of an user', async () => {
            const userId = 5;
            const response = await cartService.getCartsByUser(userId);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate the entire schema
            const schemaIsValid = validateSchema(getByIdSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);

            // validate user in response.
            const userCheck = respBody.carts.every((item) => item.userId === userId);
            expect(userCheck).toBeTruthy();
        });
    });
});

test.describe('GET carts of an user /carts/user/id negative scenario', () => {
    test('get carts of user that do not exist', async ({ cartService }) => {
        await test.step('get cart of an user', async () => {
            const userId = 66666;
            const response = await cartService.getCartsByUser(userId);
            await response.json();
            const expectedStatus = 404;
            expect(response.status()).toBe(expectedStatus);
        });
    });
});
