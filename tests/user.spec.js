const { expect } = require('@playwright/test');
const { test } = require('../src/fixtures/baseApi');
const getSchema = require('../src/util/schema/get-all-users-schema');

const id = 2;

test.describe('GET all users /users', () => {
    test('should get all users', async ({ userService, validateSchema }) => {
        await test.step('get all users', async () => {
            const response = await userService.getAllUsers();
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);
            expect(respBody.users.length).toBeGreaterThanOrEqual(1);

            // validate the entire schema
            const schemaIsValid = validateSchema(getSchema.schema, respBody);
            expect(schemaIsValid).toBe(true);
        });
    });
});

test.describe('GET a single user /users/id', () => {
    test('should get an user by id', async ({ userService }) => {
        await test.step('get an user by id', async () => {
            const response = await userService.getUserById(id);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate the id
            expect(respBody.id).toBe(id);
        });
    });
});

test.describe('GET user /users/id negative scenario', () => {
    test('negative scenario to get user by invalid id', async ({ userService }) => {
        await test.step('get an user by id', async () => {
            const userId = 88888;
            const response = await userService.getUserById(userId);
            await response.json();
            const expectedStatus = 404;
            expect(response.status()).toBe(expectedStatus);
        });
    });
});

test.describe('Filter users /filter?key = hair.color & value=Brown', () => {
    test('filter users', async ({ userService }) => {
        await test.step('filter users', async () => {
            const value = 'Brown';
            const response = await userService.filterUsers('hair.color', value);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate hair color in response.
            const hairColorCheck = respBody.users.every((item) => item.hair.color === value);
            expect(hairColorCheck).toBeTruthy();
        });
    });
});

test.describe('Filter users /filter?key = hair.color & value=inavlidValue', () => {
    test('filter users with inavlid value', async ({ userService }) => {
        await test.step('filter users', async () => {
            const value = 'kkkkk';
            const response = await userService.filterUsers('hair.color', value);
            const respBody = await response.json();
            const expectedStatus = 200;
            expect(response.status()).toBe(expectedStatus);

            // validate users in response.
            expect(respBody.users.length).toBe(0);
            expect(respBody.total).toBe(0);
        });
    });
});
