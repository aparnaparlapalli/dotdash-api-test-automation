const { expect } = require('@playwright/test');
const { test } = require('../src/fixtures/baseApi');
const {
 targetRating, expectedTitles, expectedHighestTotalCart, expectedLowestTotalCart,
} = require('./data/features.data');

test.describe('Get titles of all products that have a rating less than or equal to the provided criteria @feature', () => {
    test('validate titles of the products that have rating less than or eual to the criteria', async ({ features }) => {
        await test.step('validate titles', async () => {
            const actualTitles = await features.getProductTitlesByWorseRating(targetRating);
            expect(actualTitles).toEqual(expectedTitles);
        });
    });
});

test.describe('Returns the cart with the highest total value @feature', () => {
    test('validate the cart and return the cart with highest value', async ({ features }) => {
        await test.step('validate cart products value', async () => {
            const cartWithHighestValue = await features.getCartWithHighestTotal();
            expect(cartWithHighestValue).toEqual(expectedHighestTotalCart);
        });
    });
});

test.describe('Returns the cart with the lowest total value @feature', () => {
    test('validate the cart and return the cart with lowest value', async ({ features }) => {
        await test.step('validate cart products value', async () => {
            const cartWithLowestValue = await features.getCartWithLowestTotal();
            expect(cartWithLowestValue).toEqual(expectedLowestTotalCart);
        });
    });
});
