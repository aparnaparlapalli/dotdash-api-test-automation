class CartService {
    constructor(request) {
        this.request = request;
        this.path = '/carts';
    }

    // Get all carts
    // API endpoint to get data: https://dummyjson.com/carts
    async getAllCarts() {
        const url = `${this.path}`;
        const response = await this.request.get(url);
        return response;
    }

    // Get a single cart
    // API endpoint to get data: https://dummyjson.com/carts/{cartId}
    async getCartById(id) {
        const url = `${this.path}/${id}`;
        const response = await this.request.get(url);
        return response;
    }

    // Get carts of a user
    // API endpoint to get data: https://dummyjson.com/carts/user/{userId}
    async getCartsByUser(id) {
        const url = `${this.path}/user/${id}`;
        const response = await this.request.get(url);
        return response;
    }
}

module.exports = CartService;
