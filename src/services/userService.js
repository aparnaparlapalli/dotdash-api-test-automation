class UserService {
    constructor(request) {
        this.request = request;
        this.path = '/users';
    }

    // Retrieves a list of all users.
    // API endpoint to get data: https://dummyjson.com/users
    async getAllUsers() {
        const url = `${this.path}`;
        const response = await this.request.get(url);
        return response;
    }

    // Retrieves a single user by their ID.
    // API endpoint to get data: https://dummyjson.com/users/{userId}
    async getUserById(id) {
        const url = `${this.path}/${id}`;
        const response = await this.request.get(url);
        return response;
    }

    // Filter users by key and value
    // API endpoint to get data: https://dummyjson.com/users/filter?key=hair.color&value=Brown
    async filterUsers(key, value) {
        const url = `${this.path}/filter`;
        const response = await this.request.get(url, {
            params: {
                key,
                value,
            },
        });
        return response;
    }
}

module.exports = UserService;
