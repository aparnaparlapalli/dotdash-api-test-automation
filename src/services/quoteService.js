class QuoteService {
    constructor(request) {
        this.request = request;
        this.path = '/quotes';
    }

    // Retrieves a list of all quotes.
    // API endpoint to get data: https://dummyjson.com/quotes
    async getAllQuotes() {
        const url = `${this.path}`;
        const response = await this.request.get(url);
        return response;
    }

    // Retrieves random quote
    // API endpoint to get data: https://dummyjson.com/quotes/random
    async getRandomQuote() {
        const url = `${this.path}/random`;
        const response = await this.request.get(url);
        return response;
    }

    // Get a single quote
    // API endpoint to get data: https://dummyjson.com/quotes/{quoteId}
    async getQuoteById(id) {
        const url = `${this.path}/${id}`;
        const response = await this.request.get(url);
        return response;
    }

    // Get quotes by passing query params
    // API endpoint to get data: https://dummyjson.com/quotes?limit=3&skip=10
    async getLimitAndSkipQuotes(limit, skip) {
        const url = `${this.path}`;
        const response = await this.request.get(url, {
            params: {
                limit,
                skip,
            },
        });
        return response;
    }
}

module.exports = QuoteService;
