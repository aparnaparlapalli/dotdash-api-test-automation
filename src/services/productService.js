class ProductService {
    constructor(request) {
        this.request = request;
        this.path = '/products';
    }

    // Get all products
    // API endpoint to get data: https://dummyjson.com/products
    async getAllProducts() {
        const url = `${this.path}`;
        const response = await this.request.get(url);
        return response;
    }

    // Add a product
    // Api endpoint to add a product https://dummyjson.com/products/add
    async postProduct(payload) {
        const response = await this.request.post(`${this.path}/add`, {
            body: payload,
        });
        return response;
    }

    // Update a product
    // Api endpoint to update a product https://dummyjson.com/products/{productId}
    async putProduct(id, payload) {
        const response = await this.request.put(`${this.path}/${id}`, {
            body: payload,
        });
        return response;
    }

    // Get limit and skip products
    // Api endpoint to limit and skip products https://dummyjson.com/products?limit=10&skip=10&select=title,price'
    async getProductByQueryParam(limit, skip, select) {
        const url = `${this.path}`;
        const response = await this.request.get(url, {
            params: {
                limit,
                skip,
                select,
            },
        });
        return response;
    }

    // Get a single product
    // API endpoint to get data: https://dummyjson.com/products/{productId}
    async getProductById(id) {
        const url = `${this.path}/${id}`;
        const response = await this.request.get(url);
        return response;
    }

    // Get all products categories
    // API endpoint to get data: https://dummyjson.com/products/categories
    async getProductCategories() {
        const url = `${this.path}/categories`;
        const response = await this.request.get(url);
        return response;
    }

    // Get all products of a category
    // API endpoint to get data: https://dummyjson.com/products/category/{categoryName}
    async getProductByCategory(categoryName) {
        const url = `${this.path}/category/${categoryName}`;
        const response = await this.request.get(url);
        return response;
    }

    // Search for products in TestMart
    // API endpoint to get data: https://dummyjson.com/products/search?q={query}
    async searchProduct(q) {
        const url = `${this.path}/search`;
        const response = await this.request.get(url, {
            params: {
                q,
            },
        });
        return response;
    }

    // Delete a product
    // Api endpoint to delete a product https://dummyjson.com/products/{productId}
    async deleteProduct(id) {
        const url = `${this.path}/${id}`;
        const response = await this.request.delete(url);
        return response;
    }
}

module.exports = ProductService;
