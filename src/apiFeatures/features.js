const CartService = require('../services/cartService');
const ProductService = require('../services/productService');

class Features {
    constructor(request) {
        this.request = request;
        this.productService = new ProductService(this.request);
        this.cartService = new CartService(this.request);
    }

    /* Return titles of all products that have a rating less than or equal
     to the provided criteria */
    async getProductTitlesByWorseRating(targetRating) {
        const response = await this.productService.getAllProducts();
        const respBody = await response.json();

        // find products with rating less than the target rating
        const lowRatingProducts = respBody.products.filter(
            (product) => product.rating <= targetRating,
        );

        // Create an array of expected titles from lowRatingProducts
        const expectedTitles = lowRatingProducts.map((product) => product.title);
        return expectedTitles;
    }

    // Returns the cart with the highest total value.
    async getCartWithHighestTotal() {
        const response = await this.cartService.getAllCarts();
        const respBody = await response.json();

        // Initialize variables
        let highestValue = -1;
        let cartWithHighestValue = null;

        /* Iterate through the carts product list to find the cart
        with the highest sum of total prices */
        respBody.carts.forEach((cart) => {
            const cartTotalPriceSum = cart.products.reduce(
                (sum, product) => sum + product.total,
                0,
            );
            if (cartTotalPriceSum > highestValue) {
                highestValue = cartTotalPriceSum;
                cartWithHighestValue = cart;
            }
        });
        return cartWithHighestValue;
    }

    // Returns the cart with the lowest total value.
    async getCartWithLowestTotal() {
        const response = await this.cartService.getAllCarts();
        const respBody = await response.json();

        // Initialize variables
        let lowestValue = Number.MAX_SAFE_INTEGER;
        let cartWithLowestValue = null;

        /* Iterate through the carts product list to find the cart
        with the lowest sum of total prices */
        respBody.carts.forEach((cart) => {
            const cartTotalPriceSum = cart.products.reduce(
                (sum, product) => sum + product.total,
                0,
            );
            if (cartTotalPriceSum < lowestValue) {
                lowestValue = cartTotalPriceSum;
                cartWithLowestValue = cart;
            }
        });
        return cartWithLowestValue;
    }
}

module.exports = Features;
