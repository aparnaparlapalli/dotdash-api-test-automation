/* eslint-disable no-empty-pattern */
const { test: base } = require('@playwright/test');
const ProductService = require('../services/productService');
const CartService = require('../services/cartService');
const UserService = require('../services/userService');
const QuoteService = require('../services/quoteService');
const Features = require('../apiFeatures/features');
const validateSchema = require('../util/validate-schema');

// Extend base by providing fixtures
const test = base.extend({
    productService: async ({ request }, use) => {
        await use(new ProductService(request));
    },
    cartService: async ({ request }, use) => {
        await use(new CartService(request));
    },
    userService: async ({ request }, use) => {
        await use(new UserService(request));
    },
    quoteService: async ({ request }, use) => {
        await use(new QuoteService(request));
    },
    features: async ({ request }, use) => {
        await use(new Features(request));
    },
    validateSchema: async ({}, use) => {
        await use(validateSchema);
    },
});

module.exports = { test };
