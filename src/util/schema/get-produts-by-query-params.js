const ajv = require('../ajv');

const schema = {
    type: 'object',
    properties: {
        products: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    id: { type: 'number' },
                    title: { type: 'string' },
                    price: { type: 'number' },
                },
                required: ['id', 'title', 'price'],
            },
        },
        total: { type: 'number' },
        skip: { type: 'number' },
        limit: { type: 'number' },
    },
    required: ['products', 'total', 'skip', 'limit'],
};

module.exports = ajv.compile(schema);
