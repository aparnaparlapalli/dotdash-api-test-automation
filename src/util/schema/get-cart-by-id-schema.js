const ajv = require('../ajv');

const schema = {
    type: 'object',
    properties: {
        carts: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    id: { type: 'integer' },
                    products: {
                        type: 'array',
                        items: {
                            type: 'object',
                            properties: {
                                id: { type: 'integer' },
                                title: { type: 'string' },
                                price: { type: 'number' },
                                quantity: { type: 'integer' },
                                total: { type: 'number' },
                                discountPercentage: { type: 'number' },
                                discountedPrice: { type: 'number' },
                            },
                            required: ['id', 'title', 'price', 'quantity', 'total', 'discountPercentage', 'discountedPrice'],
                        },
                    },
                    total: { type: 'number' },
                    discountedTotal: { type: 'number' },
                    userId: { type: 'integer' },
                    totalProducts: { type: 'integer' },
                    totalQuantity: { type: 'integer' },
                },
            },
        },
        total: { type: 'number' },
        skip: { type: 'integer' },
        limit: { type: 'integer' },
    },
};

module.exports = ajv.compile(schema);
