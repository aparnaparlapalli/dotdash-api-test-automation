const ajv = require('../ajv');

const schema = {
    type: 'object',
    properties: {
        quotes: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    id: { type: 'integer' },
                    quote: { type: 'string' },
                    author: { type: 'string' },
                },
                required: ['id', 'quote', 'author'],
            },
        },
        total: { type: 'integer' },
        skip: { type: 'integer' },
        limit: { type: 'integer' },
    },
    required: ['quotes', 'total', 'skip', 'limit'],
};

module.exports = ajv.compile(schema);
