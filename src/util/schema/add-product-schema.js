const ajv = require('../ajv');

const schema = {
    type: 'object',
    properties: {
        id: { type: 'integer' },
    },
    required: ['id'],
};

module.exports = ajv.compile(schema);
