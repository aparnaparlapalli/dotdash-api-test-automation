const ajv = require('../ajv');

const schema = {
    type: 'object',
    properties: {
        users: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    id: { type: 'integer' },
                    firstName: { type: 'string' },
                    lastName: { type: 'string' },
                    maidenName: { type: 'string' },
                    age: { type: 'integer' },
                    gender: { type: 'string' },
                    email: { type: 'string' },
                    phone: { type: 'string' },
                    username: { type: 'string' },
                    password: { type: 'string' },
                    birthDate: { type: 'string' },
                    image: { type: 'string' },
                    bloodGroup: { type: 'string' },
                    height: { type: 'number' },
                    weight: { type: 'number' },
                    eyeColor: { type: 'string' },
                    hair: { type: 'object' },
                    domain: { type: 'string' },
                    ip: { type: 'string' },
                    address: { type: 'object' },
                    macAddress: { type: 'string' },
                    university: { type: 'string' },
                    bank: { type: 'object' },
                    company: { type: 'object' },
                    ein: { type: 'string', pattern: '^(\\d{2}-\\d{7})$' },
                    ssn: { type: 'string', pattern: '^(\\d{3}-\\d{2}-\\d{4})$' },
                    userAgent: { type: 'string' },
                },
                required: ['id', 'firstName', 'lastName', 'age', 'gender', 'email', 'phone', 'username', 'password', 'birthDate', 'image', 'bloodGroup', 'height', 'weight', 'eyeColor', 'hair', 'domain', 'ip', 'address', 'macAddress', 'university', 'bank', 'company', 'ein', 'ssn', 'userAgent'],
            },
        },
        total: { type: 'integer' },
        skip: { type: 'integer' },
        limit: { type: 'integer' },
    },
    required: ['users', 'total', 'skip', 'limit'],
};

module.exports = ajv.compile(schema);
