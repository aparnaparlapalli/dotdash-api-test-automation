const ajv = require('./ajv');

// function to validate defined schema against response
function validateSchema(schema, response) {
  const valid = ajv.validate(schema, response);
  if (!valid) console.log('schemaValidation', ajv.errors);
  return valid;
}

module.exports = validateSchema;
