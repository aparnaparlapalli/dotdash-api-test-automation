const Ajv = require('ajv');

const ajv = new Ajv({ allErrors: true }); // options can be passed

module.exports = ajv;
