# API automation using Playwright

This project was developed by Aparna Parlapalli and demonstrates api automation framework using Playwright, this is using Nodejs/JavaScript.
Eslint is configured to this repo.

This project automates https://dummyjson.com/

## Prerequisites

#Node JS
https://nodejs.org/en/download
```
Node.js v18.14.2
npm 9.5.0
```

#Visual studio code
https://code.visualstudio.com/download
Version: 1.78.2

### Installation
Playwright has its own test runner for end-to-end tests, we call it Playwright Test.

The easiest way to get started with Playwright Test is to run the init command.

### Run from your project's root directory
```
npm init playwright@latest
```

 ### Install eslint and init eslint to configure
 ```
 npm eslint init
 npm install --save-dev eslint-config-airbnb-base
 ```
 
### Clone repo

To run the REST API Automation test cases on your local computer : 
   1. Clone this repository: 
       git clone the repo 
   2.  Install node Packages:
       npm install

### Running tests

To run all tests
```
npm run test
npx playwright test
```

To run a specific test
```
npx playwright test <testName>
npx playwright test quote.spec.js
```

Tagging tests
Only tests with given tag are executed

```
npx playwright test --grep @feature
```

To open reports
```
npx playwright show-report
```
Trace
```
npx playwright test --trace on
```

### Skipping Tests

some tests may be skipped from running if you need to pend them out.
```
test.skip('skip test', async () => {}) to test.skip or test.describe.skip
```

### Utility scripts
```
npm run lint  - Run the lint
npm run lint:fix - Automatically fix the lint errors - run before pushing code
```

### To validate entire response json schema

Used AJV to validate the response schema

For more information https://ajv.js.org/guide/getting-started.html


### Reports
This project is configured to generate html reports. 
Once the Api Automation test execution is completed, reports can be found under the pipeline artifacts in this folder /playwright-report

# to configure allure reports 

```
npm install allure-playwright
npm install -g allure-commandline
npx allure generate ./allure-results --clean
npx allure open ./allure-report

````

### Continuious Integration on GitLab
CI/CD : Enabled GitLab CI/CD to run the API Automation part of Continuous Integration.
Refer to .gitlab-ci.yml for more information
Navigate to CI/CD Pipelines to run or review the API Automation testcases and results.

### Code snippet of parameterized cataegories test

```
test.describe('GET all products by categories', () => {
    // Parameterized categories test case
    categories.forEach((category) => {
        test(`should get all products by category "${category}"`, async ({ productService }) => {
            await test.step(`get by category "${category}"`, async () => {
                const response = await productService.getProductByCategory(category);
                const respBody = await response.json();
                const expectedStatus = 200;
                expect(response.status()).toBe(expectedStatus);
                expect(respBody.products.length).toBeGreaterThanOrEqual(1);
                const schemaIsValid = validateSchema(getSchema.schema, respBody);
                expect(schemaIsValid).toBe(true);

                // validate category in response.
                const statusCheck = respBody.products.every((item) => item.category === category);
                expect(statusCheck).toBeTruthy();
            });
        });
    });
});
```